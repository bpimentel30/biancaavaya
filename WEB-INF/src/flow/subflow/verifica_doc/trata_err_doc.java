package flow.subflow.verifica_doc;

/**
 * This servlet is used to forward the request to the entry point of a
 * project callflow (subflow).
 * Last generated by Orchestration Designer at: 26 DE FEVEREIRO DE 2019 15H26MIN20S BRT
 */
public class trata_err_doc extends com.avaya.sce.runtime.Subflow {

	//{{START:CLASS:FIELDS
	//}}END:CLASS:FIELDS

	/**
	 * Default constructor
	 * Last generated by Orchestration Designer at: 26 DE FEVEREIRO DE 2019 15H26MIN20S BRT
	 */
	public trata_err_doc() {
		//{{START:CLASS:CONSTRUCTOR
		super();
		//}}END:CLASS:CONSTRUCTOR
	}

	/**
	 * Returns the name of the subflow that is being invoked.  This name is used for
	 * determining the URL mapping for the the entry point of the subflow..
	 *
	 * @return the name of the subflow
	 * Last generated by Orchestration Designer at: 17 DE MARÇO DE 2019 23H28MIN52S BRT
	 */
	protected String getSubflowName() {
		return("trata_erro_doc");
	}
	/**
	 * Returns the name of the mapping of sub flow exit points to the URL mappings
	 * of the servlets to return back to in the calling flow.
	 *
	 * @return map of sub flow exit points to servlets in the calling flow.
	 * Last generated by Orchestration Designer at: 17 DE MARÇO DE 2019 23H28MIN52S BRT
	 */
	protected java.util.Map<String,String> getExitPoints() {
		java.util.Map<String, String> exitPoints;
		exitPoints = new java.util.HashMap<String, String>();
		exitPoints.put("trata_erro_doc-continua", "verifica_doc-coleta_doc");
		exitPoints.put("trata_erro_doc-desliga", "verifica_doc-desliga");
		return exitPoints;
	}
}
